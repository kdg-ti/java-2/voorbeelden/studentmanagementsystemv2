package be.kdg.java2.sms.service.implementation;

import be.kdg.java2.sms.database.StudentDAO;
import be.kdg.java2.sms.service.Student;
import be.kdg.java2.sms.service.StudentsService;

import java.util.List;

public class StudentsServiceImpl implements StudentsService {
    private StudentDAO studentDAO;

    public StudentsServiceImpl(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    @Override
    public List<Student> getAllStudents(){
        return studentDAO.retreiveAll();
    }

    @Override
    public void addStudent(Student student) {
        studentDAO.add(student);
    }
}
